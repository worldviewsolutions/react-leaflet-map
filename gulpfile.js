var path = require('path');
var del = require('del');
//var less = require('gulp-less');
var source = require('vinyl-source-stream');
//var react = require('gulp-react');
var gulp = require('gulp');
var $ = require('gulp-load-plugins')(); //auto-requires all "gulp-*" dependencies.  Access through $.*
var browserify = require('browserify');
var watchify = require('watchify');
var reactify = require('reactify');

// set variable via $ gulp --type production
var env = $.util.env.type || 'development';
//var isProduction = env === 'production';
var config = require('./gulpfile.config.js').getConfig(env);
var target;
var dev = target = config.paths.dev;
var dist = config.paths.dist;
var src = config.paths.src;
var srcAll = [src.js.root, src.html];

// https://github.com/ai/autoprefixer
var autoprefixerBrowsers = [                 
  'ie >= 9',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'chrome >= 34',
  'safari >= 6',
  'opera >= 23',
  'ios >= 6',
  'android >= 4.4',
  'bb >= 10'
];



// move scripts
gulp.task('scripts-copy', function() {
    return gulp
        .src(src.js.root + '**/*.js')
        .pipe(gulp.dest(target.js.root))
});

// convert jsx ==> js, move *.js
gulp.task('scripts-transform', function(){
    return browserify({
        entries: [src.js.root + src.js.entry],
        transform: [reactify],
        debug: true,
        cache: {}, packageCache: {}, fullPaths: true
    })
        .bundle().on('error', function(err){
            console.log(err.message);
            return;
        })
        .pipe(source(src.js.out))
        .pipe(gulp.dest(target.js.root)) //target.dir
});

// concat/min and move scripts
gulp.task('scripts-dist', function() {
    return browserify({
        entries: [src.js.root + src.js.entry], //path
        transform: [reactify]      
    })
        .bundle().on('error', function(err){
            console.log(err.message);
            return;
        })
        .pipe(source(dist.js.out)) //file
        .pipe($.streamify($.uglify(dist.js.out))) //file
        .pipe(gulp.dest(dist.js.root)) //dir

}); 

// move html
gulp.task('html-copy', function(){
    return gulp
        .src(src.html)
        .pipe(gulp.dest(target.html));
});

// copy html from app to out
gulp.task('html-dist', function() {
    return gulp
        .src(src.html)
        .pipe(gulp.dest(dist.html))
        .pipe($.size({ title : 'html' }))
        .pipe($.connect.reload());
});

gulp.task('html-replace', function(){
    gulp
        .src(src.html)
        .pipe($.htmlReplace({
            'js': target.js.dir + target.js.out
        }))
        .pipe(gulp.dest(target.dir));
});

// convert less to css
gulp.task('css-less', function () {
    return gulp
        .src(src.css + '**/*.less')
        .pipe($.less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest(target.css));
});

// copy images
gulp.task('images-copy', function(cb) {
    return gulp
        .src(src.images +  '**/*.{png,jpg,jpeg,gif}')
        .pipe($.size({ title : 'images' }))
        .pipe(gulp.dest(target.images));
});

// copy libs to out
gulp.task('libs-copy', function(){
    return gulp
        .src(src.libs + '**/*.*')
        .pipe(gulp.dest(target.libs))
});

// watch css, html and scripts file changes
gulp.task('watch', function() {
    gulp.watch(src.css + "**/*.less", ['css-less']);
    gulp.watch(src.html, ['html-replace']);
    var watcher  = watchify(browserify({
        entries: [src.js.root + src.js.entry],
        transform: [reactify],
        debug: true,
        cache: {}, packageCache: {}, fullPaths: true
    }));

    return watcher.on('update', function () {
        watcher
            .bundle()
            .pipe(source(src.js.out))
            .pipe(gulp.dest(target.js.root)) //target.dir
        console.log('Updated');
    })
        .bundle().on('error', function(err){
            console.log(err.message);
            return;
        })
        .pipe(source(src.js.out))
        .pipe(gulp.dest(target.js.root)); //target.dir
});

// add livereload on the given port
// not tested
gulp.task('_serve', function() {
    var serv = config.development;
    $.connect.server({
        root: target.dir,
        port: serv.port,
        base: serv.base,
        livereload: serv.liveReload
    });
});

// remove bundels
gulp.task('_clean-dev', function(cb) {
    target = config.paths.dev;
    if (target.dir.indexOf("src") > -1 ) return; //never delete source code
  return del([target.dir], cb);
});
gulp.task('_clean-dist', function(cb) {
    target = config.paths.dist;
    if (target.dir.indexOf("src") > -1 ) return; //never delete source code
    return del([target.dir], cb);
});


// by default build project and then watch files in order to trigger livereload
gulp.task('default', ['watch']);

// waits until clean is finished then builds the project
// serial execution pending in gulp 4.0.  tested plugin run-sequence, with no success
// gulp.task('build', ['build-dist'], function(){
//     gulp.start(['build-dev']);
// });

// waits until clean is finished then assembles the project for dev testing
gulp.task('build-dev', ['_clean-dev'], function(){
  target = config.paths.dev;  
  gulp.start(['images-copy', 'libs-copy', 'html-replace', 'scripts-transform', 'css-less']);
});

// waits until clean is finished then builds the project distribution
gulp.task('build-dist', ['_clean-dist'], function(){
    target = config.paths.dist;    
    gulp.start(['images-copy', 'libs-copy', 'html-replace', 'scripts-dist', 'css-less']);
});


