React Leaflet Map
=================

Overview
--------
A Basic [Leaflet.js](http://leafletjs.com/) web mapping app using [React.js](https://facebook.github.io/react/), and built with [Browserify](http://browserify.org/).This a modified version of the [react-map-boilerplate](https://github.com/frankrowe/react-map-boilerplate) project on github.
Originally setup to use npm, it has been converted to use gulp for all build scripting, as outlined [here](http://tylermcginnis.com/reactjs-tutorial-pt-2-building-react-applications-with-gulp-and-browserify/).

__Getting Started__

First, run `npm install` to install all dependencies listed in packages.json

Run `gulp build-dev`, to build concat build w/ source maps

Run `gulp build-dist` to build minified build for release 

Gulp
----
gulpfile.js build commands are organized by:

* scripts
* html
* css
* images
* libs
* '_' (private)
* build