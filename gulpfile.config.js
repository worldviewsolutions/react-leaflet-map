/**
 * Created by Jared Dunbar on 6/15/2016.
 */
//Config Options
var paths = {
    src: {
        dir: 'src/',
        js: {root:'src/scripts/', dir:'scripts/', entry: 'main.js', out: 'app.js'},
        constants: 'src/scripts/constants.js', //todo: export constants here to stand-alone file
        libs: 'src/libs/',
        html: 'src/index.html',
        css: 'src/less/',
        images: 'src/images/'
    },
    dev: {
        dir: 'dev/',
        js: {root:'dev/js/', dir: 'js/', out: 'app.js'},
        constants: 'dev/scripts/constants.js',
        libs: 'dev/libs/',
        html: 'dev/',
        css: 'dev/css/',
        images: 'dev/images/'
    },
    dist: {
        isProd: true,
        dir: 'dist/',
        js: {root:'dist/js/', dir: 'js/', out: 'app.min.js'},
        constants: 'dist/scripts/constants.js',
        libs: 'dist/libs/',
        html: 'dist/',
        css: 'dist/css/',
        images: 'dist/images/'
    }    
};

var constants = {
    default: {
        base: 'http://localhost',
        port: '63342',
        apiHost: 'http://localhost:63342'
    },
    development: {
        base: 'http://localhost',
        port: '63342',
        apiHost: 'http://localhost:63342',
        liveReload: true
    },
    staging: {
        base: 'http://vm101.worldviewsolutions.net/FEFCtest/',
        port: '',
        apiHost: 'http://vm101.worldviewsolutions.net/FEFCtest/',
    },
    production: {
        base: '',
        port: '',
        apiHost: ''
    }
};

var run = {
    development: {
        js: {
            uglify: false
        },
        css: {
            compress: false
        }
    },
    staging: {
        js: {
            uglify: true
        },
        css: {
            compress: true
        }
    },
    production: {
        js: {
            uglify: true
        },
        css: {
            compress: true
        }
    },
    default: {

        js: {
            uglify: false
        },
        css: {
            compress: false
        }
    }
};

var plugin = {
    default: {
        js: {
            uglify: {
                mangle: true
            }
        }
    },
    development: {
        js: {
            uglify: {
                mangle: false
            }
        }
    },
    staging: {
        js: {
            uglify: {
                mangle: true
            }
        }
    },
    production: {
        js: {
            uglify: {
                mangle: true
            }
        }
    }
};

module.exports.getConfig = function(env) {
    var _ = require('lodash');
    var isDev = env === 'development';
    var runOpts = _.merge( {}, run.default, run[ env ] );
    var pluginOpts = _.merge( {}, plugin.default, plugin[ env ] );
    var constantsOpts = _.merge( {}, constants.default, constants[ env ] );
    module.exports.paths = paths;
    module.exports.constants = constantsOpts;
    module.exports.run = runOpts;
    module.exports.plugin = pluginOpts;
    var config = module.exports;
    config.devtool = (isDev)? 'eval' : null;
    return config;
};