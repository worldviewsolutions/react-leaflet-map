var React = require('react');
var L = require('leaflet');

var Map = React.createClass({
  componentDidMount: function() {
    this.map = L.map(this.refs.map).setView([37.532855, -77.431561], 13);

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);

    L.marker([37.532855, -77.431561]).addTo(this.map)
      .bindPopup('World View Solutions<br> 115 South 15th Street | Suite 400')
      .openPopup();
  },
  render: function() {
    return (
      <div className="map" ref="map"></div>
    )
  }
});

module.exports = Map;
